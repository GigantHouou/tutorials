extends Area2D

export (String) var sceneName = "Level 1"

func _on_WinOrLose_body_shape_entered(body_id, body, body_shape, area_shape):
    if body.get_name() == "Player":
        get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))

