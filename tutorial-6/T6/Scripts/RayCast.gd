extends RayCast

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
var current_collider

func _ready():
    pass

func _process(delta):
    var collider = get_collider()
    
    if is_colliding() and collider is Interactable:
        if Input.is_action_just_pressed("interact"):
            collider.interact()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
